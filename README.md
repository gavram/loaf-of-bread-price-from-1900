# Loaf of Bread Price According to Inflation Rate from 1900 in USD

## Explanation

This is not the actual price of bread over time. These are the prices that should be in line with the inflation rate. The starting price for the loaf of bread was taken at $ 0.04 from 1900. According to calculations with a cumulative inflation rate, the price in today's USD should be $ 1.3. However, today the price of bread is between $ 2.2 and $ 4.4, which means that loaf of bread is more expensive today than it was in 1900.

## Data

Data obtained from:

* Starting price for loaf of bread in 1900 - <https://books.google.rs/books?id=KOIvAAAAYAAJ>
* Inflation rate - <https://www.multpl.com>

## Preparation

Data is obtained using bread_dataflows.py script from the aforementioned web addresses. The script uses the Dataflows library and creates a dataset - data.csv file and a datapackage.json file with visualization. This dataset is published at DataHub.io <https://datahub.io/gavram/loaf-of-bread-price.>
The dataset is very clean, with no missing values and except for the year and the price of bread, it contains an annual and cumulative inflation rate from 1900.

## Usage

* Clone or copy this repo on your local computer.
* Change path to folder where requirements.txt is located.
* To install requirements type or copy in your terminal: **pip install -r requirements.txt**
* To run bread_dataflows.py type or copy in your terminal: **python bread_dataflows.py**

![chart](https://gitlab.com/gavram/loaf-of-bread-price-from-1900/raw/master/chart.PNG)

## License

Creative Commons Attribution-NonCommercial 4.0 (CC-BY-NC-4.0)
<https://creativecommons.org/licenses/by-nc/4.0/>
Data and Python scripts are free to use and distribute

First change
Second change
